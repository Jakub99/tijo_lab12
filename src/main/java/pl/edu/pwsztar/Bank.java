package pl.edu.pwsztar;

import pl.edu.pwsztar.account.Account;

import java.util.ArrayList;

class Bank implements BankOperation {

    private int accountNumber = 0;

    private ArrayList<Account> accounts = new ArrayList<Account>();


    public int createAccount() {
        ++this.accountNumber;
        accounts.add(new Account(this.accountNumber));
        return this.accountNumber;
    }

    public int deleteAccount(int accountNumber) {

        for (int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getAccountNumber() == accountNumber) {
                accounts.remove(i);
                this.accountNumber--;
                return 0;
            }
        }

        return ACCOUNT_NOT_EXISTS;
    }

    public boolean deposit(int accountNumber, int amount) {
        if(amount <= 0) {
            return  false;
        }

        if(accountNumber <= 0 || accountNumber > this.accountNumber) {
            return false;
        }

        for (int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getAccountNumber() == accountNumber) {
                accounts.get(i).deposit(amount);
            }
        }

        return true;
    }

    public boolean withdraw(int accountNumber, int amount) {
        for (int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getAccountNumber() == accountNumber) {
                if (accounts.get(i).getBalance() >= Math.abs(amount)) {
                    accounts.get(i).widthdraw(amount);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        if(amount < 0 || toAccount == ACCOUNT_NOT_EXISTS || fromAccount == ACCOUNT_NOT_EXISTS) return false;

        for (int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getAccountNumber() == fromAccount) {
                if (accounts.get(i).getAccountNumber() == fromAccount && accounts.get(i).getBalance() >= amount) {
                    // send transfer
                    accounts.get(i).widthdraw(amount);
                    for (int k = 0; k < accounts.size(); k++) {
                        if(accounts.get(k).getAccountNumber() == toAccount) {
                            accounts.get(k).deposit(amount);
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public int accountBalance(int accountNumber) {
        for (int i = 0; i < accounts.size(); i++) {
            if(accounts.get(i).getAccountNumber() == accountNumber) {
                return accounts.get(i).getBalance();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        int totalBalanceSum = 0;

        for (int i = 0; i < accounts.size(); i++) {
            totalBalanceSum += accounts.get(i).getBalance();
        }

        return totalBalanceSum;
    }
}
