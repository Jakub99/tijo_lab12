package pl.edu.pwsztar.account;

public class Account {
    private int accountNumber;
    private int balance;

    public Account(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void deposit(int amount) {
        this.balance += amount;
    }

    public void widthdraw(int amount) {
        this.balance -= amount;
    }

    public int getBalance() {
        return balance;
    }
}
